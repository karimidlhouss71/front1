import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Leave } from 'src/app/Entities/leave';
import { EmployeeService } from 'src/app/service/employee.service';

import { Employee } from '../../employee/add-emplyee/employee';
import { EmployeeResponse } from '../../employee/add-emplyee/employeeResponse';
import { LeavesService } from '../../service/leaves.service';

@Component({
  selector: 'app-info-leave',
  templateUrl: './info-leave.component.html',
  styleUrls: ['./info-leave.component.css'],
})
export class InfoLeaveComponent implements OnInit {
  public leaves: Leave[]=[];
  public employee: EmployeeResponse;
  id;

  constructor(
    private leavesService: LeavesService,
    private employeeService: EmployeeService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const id = params['id'];
      this.id = params['id'];
      console.log('Url Id: ', id);
      this.employeeService.getEmployeeById(id).subscribe(
        (response: EmployeeResponse) => {
          console.log(response.leaves);

          this.employee = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    });
  }

  /*downloadLeavePdf(id, fileName): void {
    this.leavesService.downloadLeavePdf(id).subscribe((response) => {
      // let filename: string = 'file_name';
      let binaryData = [];
      binaryData.push(response.body);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(
        new Blob(binaryData, { type: 'blob' })
      );
      downloadLink.setAttribute('download', fileName);
      document.body.appendChild(downloadLink);
      downloadLink.click();
    });
  }*/
  downloadLeavePdf(id, fileName): void {
    this.leavesService.downloadLeavePdf(id).subscribe((response) => {
      console.log(response);
      const url = window.URL.createObjectURL(response.body);
      const link = document.createElement('a');
      link.href = url;
      console.log(link);
      link.setAttribute('download', fileName + '.pdf');
      document.body.appendChild(link);
      link.click();
    });
  }
}
