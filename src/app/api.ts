const API_BASE_URL = 'localhost:8080';

const request = (options) => {
  const headers = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });

  if (options.type === 'Data') {
    headers.delete('Content-Type');
    // headers.delete("Accept");
  }

  // if (localStorage.getItem(ACCESS_TOKEN)) {
  //   headers.append(
  //     'Authorization',
  //     'Bearer ' + localStorage.getItem(ACCESS_TOKEN)
  //   );
  // }

  const defaults = { headers };
  options = Object.assign({}, defaults, options);
  console.log(options);

  if (options.type === 'Download') {
    return fetch(options.url, options).then((response) => {
      if (!response.ok) {
        return Promise.reject(response);
      }
      return response;
    });
  }

  return fetch(options.url, options).then((response) =>
    response.json().then((json) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    })
  );
};

export function getLeavePdf(id): any {
  return request({
    url: API_BASE_URL + '/downloadLeave/' + id,
    method: 'POST',
    type: 'Download',
    body: null,
  });
}
