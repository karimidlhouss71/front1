import { Leave } from 'src/app/Entities/leave';
export interface Employee{

id: number;
matricule: String;
nom : String;
prenom: String;
date_naissance : Date ;
cin : String;
email : String;
tele : String;
diplome : String;
specialite : String;
hire_date : Date;
poste_budgetaire : String;
service_affectation : String;
localite : String;
photo : String;
leaves: Leave[];
department:any;



}
