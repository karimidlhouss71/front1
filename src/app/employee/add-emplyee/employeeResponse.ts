import { Department } from "src/app/Entities/department";
import { Leave } from "src/app/Entities/leave";
import { Cv } from "./cv";
import { Image } from "./image";

export interface EmployeeResponse{

  id: number;
  matricule: String;
  nom : String;
  prenom: String;
  date_naissance : Date ;
  cin : String;
  email : String;
  tele : String;
  diplome : String;
  specialite : String;
  hire_date : Date;
  poste_budgetaire : String;
  service_affectation : String;
  localite : String;
  photo : String;
  echelle:number;
  grade:String;
  leaveBalance:number;
  leaves: Leave[];
  department:Department;
  cv: Cv;
  image: Image;
  }
