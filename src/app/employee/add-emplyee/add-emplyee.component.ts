import { Router } from '@angular/router';
import { EmployeeResponse } from './employeeResponse';
import { Employee } from '../list-emplyee/employee';
import { HttpErrorResponse } from '@angular/common/http';
import { EmployeeService } from '../../service/employee.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Department } from 'src/app/Entities/department';
import { DepartmentService } from 'src/app/service/department.service';

@Component({
  selector: 'app-newemp',
  templateUrl: './add-emplyee.component.html',
  styleUrls: ['./add-emplyee.component.css']
})
export class AddEmplyeeComponent implements OnInit {

  public employees: Employee[];
  public departments:Department[];
  private employee:EmployeeResponse;
  private cv: File;
  private image: File;

  constructor(private employeeService:EmployeeService,private router:Router,
    private depService:DepartmentService) { }

  ngOnInit(): void {
  this.getDep();
  }
  public getDep():void{
    this.depService.getDepartment().subscribe(
      (response:Department[])=>{
        console.log(response);
        this.departments=response;
      },
      (error:HttpErrorResponse)=>{
        alert(error.message);
      }
    );
  }
  public getEmployees():void{
    this.employeeService.getEmployees().subscribe(
      (response:Employee[])=>{
        this.employees=response;

      },
      (error:HttpErrorResponse)=>{
        alert(error.message);
      }
    );
  }

  selectCv(event) {
    this.cv = event.target.files[0];
  }
  selectImage(event) {
    this.image = event.target.files[0];
  }
  public onAddEmployee(addForm: NgForm){
    console.log("testttt")
    console.log(addForm.value)
    addForm.value["department"] = { id: addForm.value["department"] };

    console.log(addForm.value)
    console.log(this.employee)
    delete addForm.value.cv
    delete addForm.value.image
    this.employee = addForm.value
    let data = new FormData();
    data.append('cv',this.cv);
    data.append('image',this.image);
    data.append('employee', JSON.stringify(this.employee));

    // debugger
    this.employeeService.addEmployees(data).subscribe(
      (response:EmployeeResponse)=>{
        console.log(response);
        //this.getEmployees();
        alert("l'employee est ajouté avec succees!!!!")
        //this.getEmployees();
       // window.location.reload();
       this.router.navigate(['/listEmplyee'])


      },
      (error:HttpErrorResponse)=>{
        alert("veuillez remplir tout les champs");

      }
    );
  }
   /* saveemployee(){
      this.employeeService.addEmployees().subscribe(
        (response:Employee)=>{
          console.log(response);
          this.getEmployees();

        },
        (error:HttpErrorResponse)=>{
          alert(error.message);

        }
      )
    }*/
 /* public onAddEmployee(){
    console.log('hi');
  }*/


}
