import { Employee } from '../list-emplyee/employee';
import { HttpErrorResponse } from '@angular/common/http';
import { EmployeeService } from '../../service/employee.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeResponse } from '../add-emplyee/employeeResponse';

@Component({
  selector: 'app-newemp',
  templateUrl: './edit-emplyee.component.html',
  styleUrls: ['./edit-emplyee.component.css'],
})
export class EditEmplyeeComponent implements OnInit {
  public employee: EmployeeResponse;
  public employees:Employee[];

  id

  constructor(
    private employeeService: EmployeeService,public router:Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      const id = params['id'];
      this.id = params['id'];
      console.log('Url Id: ', id);
      // console.log('activateRoute : ', this.activatedRoute);
      this.employeeService.getEmployeeById(id).subscribe(
        (response: EmployeeResponse) => {
          this.employee = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    });
  }
  public getEmployees():void{
    this.employeeService.getEmployees().subscribe(
      (response:Employee[])=>{
        this.employees=response;

      },
      (error:HttpErrorResponse)=>{
        alert(error.message);
      }
    );
  }
  public onEditEmployee(editForm: NgForm) {
    editForm.value["id"] = this.id
    editForm.value["cv"] = this.employee.cv;
    editForm.value["image"] = this.employee.image
    editForm.value["department"] = this.employee.department
    console.log(editForm.value);
    this.employeeService.updateEmployees(editForm.value).subscribe(
      (response: Employee) => {
        console.log(response);
        alert("l'employee est modifié avec succees!!!!")
        //this.getEmployees();
       // window.location.reload();
       this.router.navigate(['/listEmplyee'])

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
